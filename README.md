# Pipeline Maven Sample

## Development

Assumes that all preparation for a release is done in a release branch, when the release branch is considered stable it is merged to master.

## Release

The custom pipeline prepare-release should be run on the release branch just prior to it being merged to master or on the master branch just after the release branch is merged.

The custom pipeline prepare-release runs the maven release prepare goal.

The release prepare goal will perform the following tasks:

* Changes the version in the POM from n-SNAPSHOT to n
* Commits the modified POMs
* Tag the code with the version name
* Changes the version in the POM to (n+1)-SNAPSHOT
* Commits the modified POMs

The resulting version tag will then be built by the tag Pipeline.  The tag pipeline could deploy the version to a maven repository or deploy to a production server. 

## Refrences
Maven Release [prepare release](http://maven.apache.org/maven-release/maven-release-plugin/examples/prepare-release.html) and [perform release](http://maven.apache.org/maven-release/maven-release-plugin/examples/perform-release.html)
[Spring Boot](https://spring.io/guides/gs/spring-boot/) 